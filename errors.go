package siconla

import "fmt"

// A ParserError represents an error in the input stream of a Decoder.
type ParserError struct {
	Line    int64
	Message string
	err     error
}

// Error returns the string representation of e, in the format "line: message".
func (e *ParserError) Error() string {
	return fmt.Sprintf("%d: %s", e.Line, e.Message)
}

// Unwrap returns the underlying error, or nil if there is none.
func (e *ParserError) Unwrap() error {
	return e.err
}

func newParserError(line int64, msg string) *ParserError {
	return &ParserError{Line: line, Message: msg, err: nil}
}

func wrapParserError(line int64, err error) *ParserError {
	return &ParserError{Line: line, Message: err.Error(), err: err}
}
