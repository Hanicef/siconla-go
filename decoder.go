package siconla

import (
	"bytes"
	"errors"
	"fmt"
	"io"
	"reflect"
	"strconv"
	"strings"
	"unicode"
	"unicode/utf8"
)

// A Decoder reads Siconla data from an input stream.
type Decoder struct {
	on  []bool // Object nesting: true if object, false if array
	l   int64
	r   io.Reader
	err error
}

// NewDecoder creates a new Decoder.
func NewDecoder(r io.Reader) *Decoder {
	return &Decoder{r: r}
}

// InputLine returns the line the input stream is currently standing on.
//
// This line has already been read from the input, so the line currently being
// processed will match this value.
func (dec *Decoder) InputLine() int64 {
	return dec.l
}

func (dec *Decoder) readLine() (string, error) {
	var (
		b []byte
		c [1]byte
	)
	for {
		_, err := dec.r.Read(c[:])
		if err != nil {
			if err == io.EOF && len(b) > 0 {
				break
			}
			return "", err
		}
		if c[0] == '\n' {
			break
		}
		b = append(b, c[0])
	}
	dec.l++
	return string(b), nil
}

var escapeMap = map[byte]byte{
	'a':  '\a',
	'b':  '\b',
	'f':  '\f',
	'n':  '\n',
	'r':  '\r',
	't':  '\t',
	'v':  '\v',
	'"':  '"',
	'\'': '\'',
	'\\': '\\',
}

func parseHex(s string, max int) (int64, int) {
	var v int64 = -0x8000_0000_0000_0000
	var i int
	for i = 0; i < max && i < len(s); i++ {
		if s[i] >= '0' && s[i] <= '9' {
			v <<= 4
			v |= int64(s[i] - '0')
		} else if s[i] >= 'a' && s[i] <= 'f' {
			v <<= 4
			v |= int64(s[i] - 'a' + 10)
		} else if s[i] >= 'A' && s[i] <= 'F' {
			v <<= 4
			v |= int64(s[i] - 'A' + 10)
		} else {
			break
		}
	}
	return v, i
}

func parseString(l int64, s string) (string, string, error) {
	s = strings.TrimLeftFunc(s, unicode.IsSpace)
	if s[0] == '\'' || s[0] == '"' {
		var out []byte
		i := 1
		for i < len(s) && s[i] != s[0] {
			if s[i] == '\\' {
				if i+2 > len(s) {
					return "", "", newParserError(l, "missing closing quote")
				}
				if s[i+1] == 'x' {
					if i+3 > len(s) {
						return "", "", newParserError(l, "missing closing quote")
					}
					c, n := parseHex(s[i+2:], 2)
					if c < 0 || c > 255 {
						return "", "", newParserError(l, "malformatted hex value")
					}
					out = append(out, byte(c))
					i += 2 + n
				} else if s[i+1] == 'u' || s[i+1] == 'U' {
					if i+3 > len(s) {
						return "", "", newParserError(l, "missing closing quote")
					}
					c, n := parseHex(s[i+2:], 8)
					if c < 0 {
						return "", "", newParserError(l, "malformatted unicode value")
					}
					var p [4]byte
					nr := utf8.EncodeRune(p[:], rune(c))
					out = append(out, p[:nr]...)
					i += 2 + n
				} else {
					c, ok := escapeMap[s[i+1]]
					if !ok {
						return "", "", newParserError(l, "unknown escape sequence")
					}
					out = append(out, c)
					i += 2
				}
			} else {
				out = append(out, s[i])
				i++
			}
		}
		if i == len(s) {
			return "", "", newParserError(l, "missing closing quote")
		}
		return string(out), s[i+1:], nil
	} else {
		i := strings.IndexAny(s, "=*{[]}#")
		if i == -1 {
			return strings.TrimRightFunc(s, unicode.IsSpace), "", nil
		}
		return strings.TrimRightFunc(s[:i], unicode.IsSpace), s[i:], nil
	}
}

func (dec *Decoder) objectToken(line string) (Token, error) {
	if line[0] == '}' {
		if len(dec.on) == 0 {
			dec.err = newParserError(dec.l, "mismatching closing bracket")
			return nil, dec.err
		}
		if strings.TrimSpace(line[1:]) != "" {
			dec.err = newParserError(dec.l, "expected EOL")
			return nil, dec.err
		}
		dec.on = dec.on[:len(dec.on)-1]
		return EndObject{}, nil
	} else if line[0] == ']' {
		dec.err = newParserError(dec.l, "mismatching closing bracket")
		return nil, dec.err
	}

	k, line, err := parseString(dec.l, line)
	if err != nil {
		dec.err = err
		return nil, err
	}
	line = strings.TrimLeftFunc(line, unicode.IsSpace)
	if len(line) == 0 {
		dec.err = newParserError(dec.l, "missing value")
		return nil, dec.err
	}

	tok, err := dec.getValue(line)
	if err != nil {
		return nil, err
	}

	return Value{Name: k, Value: tok}, nil
}

func (dec *Decoder) arrayToken(line string) (Token, error) {
	switch line[0] {
	case '}':
		dec.err = newParserError(dec.l, "mismatching closing bracket")
		return nil, dec.err
	case ']':
		if strings.TrimSpace(line[1:]) != "" {
			dec.err = newParserError(dec.l, "expected EOL")
			return nil, dec.err
		}
		dec.on = dec.on[:len(dec.on)-1]
		return EndArray{}, nil
	default:
		return dec.getValue(line)
	}
}

func (dec *Decoder) getValue(line string) (Token, error) {
	switch line[0] {
	case '{':
		if strings.TrimSpace(line[1:]) != "" {
			dec.err = newParserError(dec.l, "expected EOL")
			return nil, dec.err
		}
		dec.on = append(dec.on, true)
		return Object{}, nil
	case '[':
		if strings.TrimSpace(line[1:]) != "" {
			dec.err = newParserError(dec.l, "expected EOL")
			return nil, dec.err
		}
		dec.on = append(dec.on, false)
		return Array{}, nil
	case '=', '*':
		s, end, err := parseString(dec.l, line[1:])
		if err != nil {
			dec.err = err
			return nil, err
		}
		if end != "" {
			dec.err = newParserError(dec.l, "expected EOL")
			return nil, dec.err
		}
		return s, nil
	default:
		dec.err = newParserError(dec.l, "unknown token")
		return nil, dec.err
	}
}

// Token retrieves the next token in the stream. At the end of the input stream,
// Token return nil, io.EOF.
//
// Token guarantees that each start and end token match and are properly nested:
// if any mismatch occurs during parsing, Token returns an error.
//
// If Token returns an error, subsequent calls to Token on the same object will
// return the same error.
func (dec *Decoder) Token() (Token, error) {
	var (
		line string
		err  error
	)
	if dec.err != nil {
		return nil, dec.err
	}
	for line == "" {
		line, err = dec.readLine()
		if err != nil {
			if errors.Is(err, io.EOF) && len(dec.on) != 0 {
				dec.err = newParserError(dec.l, "missing closing bracket")
				return nil, dec.err
			}
			dec.err = err
			return nil, err
		}
		line = strings.TrimLeftFunc(line, unicode.IsSpace)
		if len(line) > 0 && line[0] == '#' {
			line = ""
		}
	}

	if len(dec.on) == 0 || dec.on[len(dec.on)-1] {
		return dec.objectToken(line)
	} else {
		return dec.arrayToken(line)
	}
}

// Unmarshaler is an interface implemented by objects that can unmarshal a
// Siconla element to themselves.
//
// UnmarshalSiconla decodes a single Siconla token beginning with the given
// start token. If it returns an error, the outer call to Unmarshal stops and
// returns that error. UnmarshalSiconla must parse the entire Siconla element,
// except if it returns an error.
//
// UnmarshalSiconla should not add a line number to the error; Unmarshal will do
// so by wrapping it in a ParserError.
type Unmarshaler interface {
	UnmarshalSiconla(dec *Decoder, tok Token) error
}

func decodeType(dec *Decoder, tok Token, v reflect.Value) error {
	var m reflect.Value
	unmarshalerType := reflect.TypeOf((*Unmarshaler)(nil)).Elem()
	if v.Type().Implements(unmarshalerType) {
		m = v.MethodByName("UnmarshalSiconla")
	} else if v.Addr().Type().Implements(unmarshalerType) {
		m = v.Addr().MethodByName("UnmarshalSiconla")
	}
	if m.IsValid() {
		args := []reflect.Value{reflect.ValueOf(dec), reflect.ValueOf(tok)}
		ret := m.Call(args)
		err := ret[0].Interface()
		if err != nil {
			return err.(error)
		}
		return nil
	}
	f := typeDecoder[v.Kind()]
	if f == nil {
		panic(fmt.Sprintf("cannot unmarshal type %v", v.Kind()))
	}
	return f(dec, tok, v)
}

func decodeStruct(dec *Decoder, tok Token, v reflect.Value) error {
	if _, ok := tok.(Object); !ok {
		return errors.New("expected object")
	}
	for {
		tok, err := dec.Token()
		if err != nil {
			if errors.Is(err, io.EOF) {
				break
			}
			return err
		}
		if _, ok := tok.(EndObject); ok {
			break
		}
		val := tok.(Value)
		f := v.FieldByName(val.Name)
		if !f.IsValid() {
			continue
		}
		err = decodeType(dec, val.Value, f)
		if err != nil {
			return err
		}
	}
	return nil
}

func decodeSlice(dec *Decoder, tok Token, v reflect.Value) error {
	if _, ok := tok.(Array); !ok {
		return errors.New("expected array")
	}
	a := reflect.MakeSlice(v.Type(), 0, 0)
	f := reflect.New(v.Type().Elem()).Elem()
	for {
		tok, err := dec.Token()
		if err != nil {
			return err
		}
		if _, ok := tok.(EndArray); ok {
			break
		}
		err = decodeType(dec, tok, f)
		if err != nil {
			return err
		}
		a = reflect.Append(a, f)
	}
	v.Set(a)
	return nil
}

func decodeArray(dec *Decoder, tok Token, v reflect.Value) error {
	if _, ok := tok.(Array); !ok {
		return errors.New("expected array")
	}
	for i := 0; i < v.Type().Len(); i++ {
		tok, err := dec.Token()
		if err != nil {
			return err
		}
		if _, ok := tok.(EndArray); ok {
			return nil
		}
		f := v.Index(i)
		err = decodeType(dec, tok, f)
		if err != nil {
			return err
		}
	}
	tok, err := dec.Token()
	if err != nil {
		return err
	}
	if _, ok := tok.(EndArray); ok {
		return nil
	}
	return errors.New("too many values")
}

func decodeMap(dec *Decoder, tok Token, v reflect.Value) error {
	if _, ok := tok.(Object); !ok {
		return errors.New("expected object")
	}
	m := reflect.MakeMap(v.Type())
	f := reflect.New(v.Type().Elem()).Elem()
	k := reflect.New(v.Type().Key()).Elem()
	for {
		tok, err := dec.Token()
		if err != nil {
			if errors.Is(err, io.EOF) {
				break
			}
			return err
		}
		if _, ok := tok.(EndObject); ok {
			break
		}
		val := tok.(Value)
		err = decodeType(dec, val.Value, f)
		if err != nil {
			return err
		}
		err = decodeType(dec, val.Name, k)
		if err != nil {
			return err
		}
		m.SetMapIndex(k, f)
	}
	v.Set(m)
	return nil
}

func decodePtr(dec *Decoder, tok Token, v reflect.Value) error {
	f := reflect.New(v.Type().Elem())
	err := decodeType(dec, tok, f.Elem())
	if err != nil {
		return err
	}
	v.Set(f)
	return nil
}

func decodeBool(dec *Decoder, tok Token, v reflect.Value) error {
	if s, ok := tok.(string); ok {
		if len(s) == 0 {
			return fmt.Errorf("cannot set value '%s' to bool", s)
		}
		var b bool
		switch s[0] {
		case 'y', 't', 'Y', 'T':
			b = true
		case 'n', 'f', 'N', 'F':
			b = false
		default:
			return fmt.Errorf("cannot set value '%s' to bool", s)
		}
		v.Set(reflect.ValueOf(b).Convert(v.Type()))
		return nil
	}
	return errors.New("expected string")
}

func decodeInt(size int) func(dec *Decoder, tok Token, v reflect.Value) error {
	return func(dec *Decoder, tok Token, v reflect.Value) error {
		if s, ok := tok.(string); ok {
			i, err := strconv.ParseInt(s, 0, size)
			if err != nil {
				return fmt.Errorf("cannot set value '%s' to int", s)
			}
			v.Set(reflect.ValueOf(i).Convert(v.Type()))
			return nil
		}
		return errors.New("expected string")
	}
}

func decodeUint(size int) func(dec *Decoder, tok Token, v reflect.Value) error {
	return func(dec *Decoder, tok Token, v reflect.Value) error {
		if s, ok := tok.(string); ok {
			i, err := strconv.ParseUint(s, 0, size)
			if err != nil {
				return fmt.Errorf("cannot set value '%s' to uint", s)
			}
			v.Set(reflect.ValueOf(i).Convert(v.Type()))
			return nil
		}
		return errors.New("expected string")
	}
}

func decodeFloat(size int) func(dec *Decoder, tok Token, v reflect.Value) error {
	return func(dec *Decoder, tok Token, v reflect.Value) error {
		if s, ok := tok.(string); ok {
			i, err := strconv.ParseFloat(s, size)
			if err != nil {
				return fmt.Errorf("cannot set value '%s' to float", s)
			}
			v.Set(reflect.ValueOf(i).Convert(v.Type()))
			return nil
		}
		return errors.New("expected string")
	}
}

func decodeString(dec *Decoder, tok Token, v reflect.Value) error {
	if s, ok := tok.(string); ok {
		v.Set(reflect.ValueOf(s).Convert(v.Type()))
		return nil
	}
	return errors.New("expected string")
}

type decodeFunc func(dec *Decoder, tok Token, v reflect.Value) error

var typeDecoder map[reflect.Kind]decodeFunc

func init() {
	typeDecoder = map[reflect.Kind]decodeFunc{
		reflect.Bool:    decodeBool,
		reflect.Int:     decodeInt(0),
		reflect.Int8:    decodeInt(8),
		reflect.Int16:   decodeInt(16),
		reflect.Int32:   decodeInt(32),
		reflect.Int64:   decodeInt(64),
		reflect.Uint:    decodeUint(0),
		reflect.Uint8:   decodeUint(8),
		reflect.Uint16:  decodeUint(16),
		reflect.Uint32:  decodeUint(32),
		reflect.Uint64:  decodeUint(64),
		reflect.Float32: decodeFloat(32),
		reflect.Float64: decodeFloat(64),
		reflect.Array:   decodeArray,
		reflect.Map:     decodeMap,
		reflect.Ptr:     decodePtr,
		reflect.Slice:   decodeSlice,
		reflect.Struct:  decodeStruct,
		reflect.String:  decodeString,
	}
}

// Decode reads the next Siconla-encoded value in the input stream and stores it
// in the value v.
//
// See the documentation for Unmarshal for details.
func (dec *Decoder) Decode(v interface{}) error {
	r := reflect.ValueOf(v)
	if r.Kind() != reflect.Ptr {
		panic("v must be a pointer")
	}
	var tok Token
	if len(dec.on) == 0 || dec.on[len(dec.on)-1] {
		tok = Object{}
	} else {
		tok = Array{}
	}
	err := decodeType(dec, tok, r.Elem())
	var pe *ParserError
	if err != nil && !errors.As(err, &pe) {
		return wrapParserError(dec.l, err)
	}
	return err
}

// Unmarshal parses the Siconla-encoded data and stores the result in the value
// pointed by v. Unmarshal panics if v is nil or not a pointer to a struct.
//
// Because Unmarshal uses the reflect package, it can only assign to exported
// (upper case) fields.
//
// Unmarshal uses the following rules when unmarshaling:
//
// When unmarshaling into a pointer, map or slice, a value is first allocated
// for that field. If a value is already stored in that field, Unmarshal will
// replace it.
//
// When unmarshaling into an int or float, Unmarshal will attempt to parse the
// value to the corresponding type. If the parsing fails, Unmarshal returns an
// error.
//
// When unmarshaling into a bool, Unmarshal checks the first character of the
// string. If the first character is y, Y, t or T, Unmarshal encodes true. If
// the first character is n, N, f or F, Unmarshal encodes false. Otherwise,
// Unmarshal returns an error.
func Unmarshal(data []byte, v interface{}) error {
	dec := NewDecoder(bytes.NewReader(data))
	return dec.Decode(v)
}
