# siconla-go - Siconla implementation for Go.

Siconla is a configuration language designed to be simple. The name is short for
"simple configuration language".

## Syntax

Each value is represented with a key-value pair, delimeted by =:

```
key = value
```

Keys and values may also contain spaces, and can also be quoted:

```
long key = long value
'long key' = 'long value'
"long key" = "long value"
```

Quoted values also have escape sequences:

```
"\tHello" = 'world\n'
```

The following list is all available escape sequences, and their representation:

```
\a -> U+0007 (bell)
\b -> U+0008 (backspace)
\f -> U+000C (form feed)
\n -> U+000A (line feed)
\r -> U+000D (carriage return)
\t -> U+0009 (horizontal tab)
\v -> U+000B (vertical tab)
\\ -> U+005C (backslash)
\' -> U+0027 (single quote)
\" -> U+0022 (double quote)
\xn -> Hex value, where n is the hex value
\un, \Un -> Unicode value, where n is the unicode value
```

Comments are lines beginning with #:

```
# This is a comment
key = value
```

Note that each comment must be on it's own separate line, so this is not
possible:

```
key = value  # Syntax error
```

Objects and arrays are represented with { ... } and [ ... ], respectively:

```
foo {
	a = b
}
bar [
	* c
]
```

Note that each line is parsed individually, so this is not possible:

```
foo
{
	# Syntax error
}
bar
[
	# Syntax error
]
baz { }
faz [ ]
```

A complete example:

```
listen = :80
routes [
	{
		host = example.com
		serve = /srv/com
	}
	{
		host = example.net
		serve = /srv/net
	}
]
default_messages {
	bad_host = "404 not found\n\nThis host is not on this server"
	bad_route = "404 not found\n\nThis route doesn't exist"
}
blocked_hosts [
	* 127.0.0.1
	* 0.0.0.0
]
```

## Install

To install, run:

```
go get gitlab.com/Hanicef/siconla-go
```

The package can then be imported through:

```
import "gitlab.com/Hanicef/siconla-go"
```

For documentation on the API, run:

```
go doc gitlab.com/Hanicef/siconla-go
```
