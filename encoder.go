package siconla

import (
	"bytes"
	"errors"
	"fmt"
	"io"
	"reflect"
	"strconv"
	"unicode"
	"unicode/utf8"
)

// An Encoder writes Siconla data to an output stream.
type Encoder struct {
	w              io.Writer
	on             []bool // Object nesting: true if object, false if array
	prefix, indent string
}

// NewEncoder creates a new Encoder.
func NewEncoder(w io.Writer) *Encoder {
	return &Encoder{w: w, prefix: "", indent: "\t"}
}

// Indent sets the indentation for the generated Siconla. All lines are
// prepended by prefix, followed by one or more indent based on nesting depth.
func (enc *Encoder) Indent(prefix, indent string) {
	enc.prefix = prefix
	enc.indent = indent
}

var unescapeMap = map[rune]byte{
	'\a': 'a',
	'\b': 'b',
	'\f': 'f',
	'\n': 'n',
	'\r': 'r',
	'\t': 't',
	'\v': 'v',
	'"':  '"',
	'\\': '\\',
}

func formatString(s string) []byte {
	var buf [4]byte
	out := make([]byte, 0, len(s)+2)
	out = append(out, '"')
	for len(s) != 0 {
		r, l := utf8.DecodeRuneInString(s)
		s = s[l:]
		if e, ok := unescapeMap[r]; ok {
			out = append(out, '\\', e)
		} else if unicode.IsPrint(r) {
			l = utf8.EncodeRune(buf[:], r)
			out = append(out, buf[:l]...)
		} else if r <= 0xff {
			out = append(out, fmt.Sprintf("\\x%x", r)...)
		} else {
			out = append(out, fmt.Sprintf("\\u%x", r)...)
		}
	}
	return append(out, '"')
}

func (enc *Encoder) encodeObject(line []byte, tok Token) error {
	if _, ok := tok.(EndObject); ok {
		if len(enc.on) == 0 {
			return errors.New("mismatching closing bracket")
		}
		enc.on = enc.on[:len(enc.on)-1]
		line = append(line, '}', '\n')
		_, err := enc.w.Write(line)
		return err
	} else if _, ok := tok.(EndArray); ok {
		return errors.New("mismatching closing bracket")
	}

	if len(enc.on) > 0 {
		line = append(line, enc.indent...)
	}

	v, ok := tok.(Value)
	if !ok {
		return errors.New("only values can be encoded in objects")
	}

	line = append(line, formatString(v.Name)...)
	tok = v.Value

	if _, ok := tok.(Object); ok {
		enc.on = append(enc.on, true)
		line = append(line, ' ', '{', '\n')
	} else if _, ok := tok.(Array); ok {
		enc.on = append(enc.on, false)
		line = append(line, ' ', '[', '\n')
	} else if s, ok := tok.(string); ok {
		line = append(line, ' ', '=', ' ')
		line = append(line, formatString(s)...)
		line = append(line, '\n')
	} else {
		return errors.New("cannot encode this token here")
	}
	_, err := enc.w.Write(line)
	return err
}

func (enc *Encoder) encodeArray(line []byte, tok Token) error {
	if _, ok := tok.(EndObject); ok {
		return errors.New("mismatching closing bracket")
	} else if _, ok := tok.(EndArray); ok {
		enc.on = enc.on[:len(enc.on)-1]
		line = append(line, ']', '\n')
		_, err := enc.w.Write(line)
		return err
	}

	if len(enc.on) > 0 {
		line = append(line, enc.indent...)
	}

	if _, ok := tok.(Object); ok {
		enc.on = append(enc.on, true)
		line = append(line, '{', '\n')
	} else if _, ok := tok.(Array); ok {
		enc.on = append(enc.on, false)
		line = append(line, '[', '\n')
	} else if s, ok := tok.(string); ok {
		line = append(line, '*', ' ')
		line = append(line, formatString(s)...)
		line = append(line, '\n')
	} else {
		return errors.New("cannot encode this token here")
	}
	_, err := enc.w.Write(line)
	return err
}

// EncodeToken encodes a single Siconla token to the output stream. If
// EncodeToken returns an error, nothing is written to the output stream.
func (enc *Encoder) EncodeToken(tok Token) error {
	line := []byte(enc.prefix)
	for i := 0; i < len(enc.on)-1; i++ {
		line = append(line, enc.indent...)
	}

	if len(enc.on) == 0 || enc.on[len(enc.on)-1] {
		return enc.encodeObject(line, tok)
	} else {
		return enc.encodeArray(line, tok)
	}
}

// Finish closes all remaining Siconla arrays and objects according to their
// nesting.
func (enc *Encoder) Finish() (err error) {
	for len(enc.on) > 0 {
		if enc.on[len(enc.on)-1] {
			err = enc.EncodeToken(EndObject{})
		} else {
			err = enc.EncodeToken(EndArray{})
		}
		if err != nil {
			return
		}
	}
	return
}

// Unmarshaler is an interface implemented by objects that can marshal
// themselves into Siconla elements.
//
// MarshalSiconla encodes a single Siconla token beginning with the given start
// token. If it returns an error, the outer call to Marshal stops and returns
// that error.
//
// MarshalSiconla must write tok to the output stream if it's not nil,
// completing it with relevant data if necessary. This is most easily handled
// as such:
//
//     s := <value>
//     var err error
//     if tok != nil {
//         val := tok.(Value)
//         val.Value = s
//         err = enc.EncodeToken(val)
//     } else {
//         err = enc.EncodeToken(s)
//     }
//     if err != nil {
//         return err
//     }
//
// where s is the value that is either the start tag (Object{} or Array{}), or
// is the string value to encode.
type Marshaler interface {
	MarshalSiconla(enc *Encoder, tok Token) error
}

func encodeType(enc *Encoder, tok Token, v reflect.Value) error {
	var m reflect.Value
	marshalerType := reflect.TypeOf((*Marshaler)(nil)).Elem()
	if v.Type().Implements(marshalerType) {
		m = v.MethodByName("MarshalSiconla")
	} else if v.CanAddr() && v.Addr().Type().Implements(marshalerType) {
		m = v.Addr().MethodByName("MarshalSiconla")
	}
	if m.IsValid() {
		var rtok reflect.Value
		if _, ok := tok.(Value); ok {
			rtok = reflect.ValueOf(tok)
		} else {
			rtok = reflect.Zero(reflect.TypeOf((*Token)(nil)).Elem())
		}
		args := []reflect.Value{reflect.ValueOf(enc), rtok}
		ret := m.Call(args)
		err := ret[0].Interface()
		if err != nil {
			return err.(error)
		}
		return nil
	}
	f := typeEncoder[v.Kind()]
	return f(enc, tok, v)
}

func encodeStruct(enc *Encoder, tok Token, v reflect.Value) error {
	if tok != nil {
		var err error
		if val, ok := tok.(Value); ok {
			val.Value = Object{}
			err = enc.EncodeToken(val)
		} else {
			err = enc.EncodeToken(Object{})
		}
		if err != nil {
			return err
		}
	}
	for i := 0; i < v.NumField(); i++ {
		f := v.Field(i)
		val := Value{Name: v.Type().Field(i).Name, Value: nil}
		err := encodeType(enc, val, f)
		if err != nil {
			return err
		}
	}
	if tok != nil {
		return enc.EncodeToken(EndObject{})
	}
	return nil
}

func encodeArray(enc *Encoder, tok Token, v reflect.Value) error {
	if tok != nil {
		var err error
		if val, ok := tok.(Value); ok {
			val.Value = Array{}
			err = enc.EncodeToken(val)
		} else {
			err = enc.EncodeToken(Array{})
		}

		if err != nil {
			return err
		}
	}
	for i := 0; i < v.Len(); i++ {
		f := v.Index(i)
		err := encodeType(enc, Array{}, f)
		if err != nil {
			return err
		}
	}
	return enc.EncodeToken(EndArray{})
}

func encodeSlice(enc *Encoder, tok Token, v reflect.Value) error {
	if v.IsNil() {
		return nil
	}
	return encodeArray(enc, tok, v)
}

func encodeMap(enc *Encoder, tok Token, v reflect.Value) error {
	if v.IsNil() {
		return nil
	}
	if tok != nil {
		var err error
		if val, ok := tok.(Value); ok {
			val.Value = Object{}
			err = enc.EncodeToken(val)
		} else if len(enc.on) > 0 {
			err = enc.EncodeToken(Object{})
		}

		if err != nil {
			return err
		}
	}
	for _, k := range v.MapKeys() {
		f := v.MapIndex(k)
		s := fmt.Sprint(k)
		val := Value{Name: s, Value: nil}
		err := encodeType(enc, val, f)
		if err != nil {
			return err
		}
	}
	if tok != nil {
		return enc.EncodeToken(EndObject{})
	}
	return nil
}

func encodePtr(enc *Encoder, tok Token, v reflect.Value) error {
	if v.IsNil() {
		return nil
	}
	return encodeType(enc, tok, v.Elem())
}

func encodeBool(enc *Encoder, tok Token, v reflect.Value) error {
	var s string
	if v.Bool() {
		s = "true"
	} else {
		s = "false"
	}
	if val, ok := tok.(Value); ok {
		val.Value = s
		return enc.EncodeToken(val)
	} else {
		return enc.EncodeToken(s)
	}
}

func encodeInt(enc *Encoder, tok Token, v reflect.Value) error {
	if val, ok := tok.(Value); ok {
		val.Value = strconv.FormatInt(v.Int(), 10)
		return enc.EncodeToken(val)
	} else {
		return enc.EncodeToken(strconv.FormatInt(v.Int(), 10))
	}
}

func encodeUint(enc *Encoder, tok Token, v reflect.Value) error {
	if val, ok := tok.(Value); ok {
		val.Value = strconv.FormatUint(v.Uint(), 10)
		return enc.EncodeToken(val)
	} else {
		return enc.EncodeToken(strconv.FormatUint(v.Uint(), 10))
	}
}

func encodeFloat(enc *Encoder, tok Token, v reflect.Value) error {
	if val, ok := tok.(Value); ok {
		val.Value = strconv.FormatFloat(v.Float(), 'f', -1, 64)
		return enc.EncodeToken(val)
	} else {
		return enc.EncodeToken(strconv.FormatFloat(v.Float(), 'f', -1, 64))
	}
}

func encodeString(enc *Encoder, tok Token, v reflect.Value) error {
	if val, ok := tok.(Value); ok {
		val.Value = v.String()
		return enc.EncodeToken(val)
	} else {
		return enc.EncodeToken(v.String())
	}
}

type encodeFunc func(enc *Encoder, tok Token, v reflect.Value) error

var typeEncoder map[reflect.Kind]encodeFunc

func init() {
	typeEncoder = map[reflect.Kind]encodeFunc{
		reflect.Bool:    encodeBool,
		reflect.Int:     encodeInt,
		reflect.Int8:    encodeInt,
		reflect.Int16:   encodeInt,
		reflect.Int32:   encodeInt,
		reflect.Int64:   encodeInt,
		reflect.Uint:    encodeUint,
		reflect.Uint8:   encodeUint,
		reflect.Uint16:  encodeUint,
		reflect.Uint32:  encodeUint,
		reflect.Uint64:  encodeUint,
		reflect.Float32: encodeFloat,
		reflect.Float64: encodeFloat,
		reflect.Array:   encodeArray,
		reflect.Map:     encodeMap,
		reflect.Ptr:     encodePtr,
		reflect.Slice:   encodeSlice,
		reflect.Struct:  encodeStruct,
		reflect.String:  encodeString,
	}
}

// Encode writes the next Siconla encoding of v to the stream.
//
// Encode does not wrap the output in brackets; it is required that previous
// calls to EncodeToken wraps the objects appropriately.
//
// See the documentation for Marshal for details.
func (enc *Encoder) Encode(v interface{}) error {
	return encodeType(enc, nil, reflect.ValueOf(v))
}

// Marshal returns the Siconla encoding of v.
//
// Marshal uses the following encoding rules:
//
// Boolean values are encoded as either the strings "true" or "false".
//
// Integers and floating-point values are encoded as their string
// representation, wrapped in quotes.
//
// String values are encoded as-is, with non-displayable characters escaped.
//
// Array and slice values are encoded as Siconla arrays.
//
// Struct values and maps are encoded as Siconla objects. Each field or key
// becomes a key to their corresponding value, and are wrapped in quotes. Map
// keys also escapes non-displayable characters.
func Marshal(v interface{}) ([]byte, error) {
	var buf bytes.Buffer
	err := NewEncoder(&buf).Encode(v)
	return buf.Bytes(), err
}
