// Package siconla implements encoding and decoding of Siconla. The mapping
// between Siconla and Go values are described in the documentation of Marshal
// and Unmarshal.
//
// See the README.md at https://gitlab.com/Hanicef/siconla-go for documentation
// on the Siconla format.
package siconla
