package siconla

import (
	"bytes"
	"errors"
	"testing"
)

const simpleExpect = `"A" = "foo"
"B" {
	"C" = "bar"
	"D" [
		* "baz"
	]
}
"E" [
	* "faz"
]
`

const indentExpect = ` "A" = "foo"
 "B" [
  * "C"
 ]
`

func TestEncodeToken(t *testing.T) {
	t.Parallel()
	t.Run("Simple", func(t *testing.T) {
		t.Parallel()
		var buf bytes.Buffer
		enc := NewEncoder(&buf)
		enc.EncodeToken(Value{Name: "A", Value: "foo"})

		enc.EncodeToken(Value{Name: "B", Value: Object{}})
		enc.EncodeToken(Value{Name: "C", Value: "bar"})
		enc.EncodeToken(Value{Name: "D", Value: Array{}})
		enc.EncodeToken("baz")
		enc.EncodeToken(EndArray{})
		enc.EncodeToken(EndObject{})

		enc.EncodeToken(Value{Name: "E", Value: Array{}})
		enc.EncodeToken("faz")
		enc.EncodeToken(EndArray{})
		if string(buf.Bytes()) != simpleExpect {
			t.Fail()
		}
	})
	t.Run("String formatting", func(t *testing.T) {
		t.Parallel()
		var buf bytes.Buffer
		enc := NewEncoder(&buf)
		enc.EncodeToken(Value{Name: "A", Value: "\a\b\f\n\r\t\v\"\\"})
		enc.EncodeToken(Value{Name: "B", Value: "\x01\x17\u2003\U0001fffe"})
		if string(buf.Bytes()) != `"A" = "\a\b\f\n\r\t\v\"\\"`+"\n"+`"B" = "\x1\x17\u2003\u1fffe"`+"\n" {
			t.Error(string(buf.Bytes()))
		}
	})
	t.Run("Missing open bracket", func(t *testing.T) {
		t.Parallel()
		var buf bytes.Buffer
		enc := NewEncoder(&buf)
		err := enc.EncodeToken(EndArray{})
		if err == nil {
			t.Fail()
		} else if err.Error() != "mismatching closing bracket" {
			t.Error(err)
		}
		err = enc.EncodeToken(EndObject{})
		if err == nil {
			t.Fail()
		} else if err.Error() != "mismatching closing bracket" {
			t.Error(err)
		}
	})
	t.Run("Bracket mismatch", func(t *testing.T) {
		t.Parallel()
		var buf bytes.Buffer
		enc := NewEncoder(&buf)
		enc.EncodeToken(Value{Name: "A", Value: Object{}})
		err := enc.EncodeToken(EndArray{})
		if err == nil {
			t.Fail()
		} else if err.Error() != "mismatching closing bracket" {
			t.Error(err)
		}
		enc.EncodeToken(Value{Name: "A", Value: Array{}})
		err = enc.EncodeToken(EndObject{})
		if err == nil {
			t.Fail()
		} else if err.Error() != "mismatching closing bracket" {
			t.Error(err)
		}
	})
	t.Run("Misplaced token", func(t *testing.T) {
		t.Parallel()
		var buf bytes.Buffer
		enc := NewEncoder(&buf)
		err := enc.EncodeToken("A")
		if err == nil {
			t.Fail()
		} else if err.Error() != "only values can be encoded in objects" {
			t.Error(err)
		}

		err = enc.EncodeToken(Value{Name: "A", Value: Value{Name: "B", Value: "C"}})
		if err == nil {
			t.Fail()
		} else if err.Error() != "cannot encode this token here" {
			t.Error(err)
		}

		enc.EncodeToken(Value{Name: "A", Value: Array{}})
		err = enc.EncodeToken(Value{Name: "A", Value: "B"})
		if err == nil {
			t.Fail()
		} else if err.Error() != "cannot encode this token here" {
			t.Error(err)
		}
	})
	t.Run("Indentation", func(t *testing.T) {
		t.Parallel()
		var buf bytes.Buffer
		enc := NewEncoder(&buf)
		enc.Indent(" ", " ")
		enc.EncodeToken(Value{Name: "A", Value: "foo"})
		enc.EncodeToken(Value{Name: "B", Value: Array{}})
		enc.EncodeToken("C")
		enc.EncodeToken(EndArray{})
		if string(buf.Bytes()) != indentExpect {
			t.Fail()
		}
	})
}

type ListObject struct {
	B [1]bool
	I [1]int
	U [1]uint
	F [1]float32
	S [1]string
	O [1]struct{ I int }
	L [1][1]string
	M [1]map[string]string
}

const marshalExpect = `"T" = "true"
"F" = "false"
"I" = "1"
"I8" = "-2"
"I16" = "3"
"I32" = "-4"
"I64" = "5"
"U" = "6"
"U8" = "7"
"U16" = "8"
"U32" = "9"
"U64" = "10"
"F32" = "10.5"
"F64" = "10.25"
"S" = "hello"
"A" [
	* "1"
	* "2"
	* "3"
]
"EA" [
]
"L" [
	* "1"
	* "2"
	* "3"
]
"O" {
	"I" = "0"
}
"P" = "1"
"M" {
	"1" = "1"
}
"C" = "foo"
`

const listExpect = `"B" [
	* "true"
]
"I" [
	* "1"
]
"U" [
	* "1"
]
"F" [
	* "1.5"
]
"S" [
	* "foo"
]
"O" [
	{
		"I" = "0"
	}
]
"L" [
	[
		* "bar"
	]
]
"M" [
	{
		"A" = "B"
	}
]
`

func (c *CustomType) MarshalSiconla(enc *Encoder, tok Token) error {
	if c == nil {
		return nil
	}
	if tok == nil {
		return enc.EncodeToken(string(*c))
	}
	val := tok.(Value)
	val.Value = string(*c)
	return enc.EncodeToken(val)
}

func TestMarshal(t *testing.T) {
	t.Run("Simple", func(t *testing.T) {
		t.Parallel()
		i := 1
		v := TestObject{
			T:   true,
			F:   false,
			I:   1,
			I8:  -2,
			I16: 3,
			I32: -4,
			I64: 5,
			U:   6,
			U8:  7,
			U16: 8,
			U32: 9,
			U64: 10,
			F32: 10.5,
			F64: 10.25,
			S:   "hello",
			A:   [3]int{1, 2, 3},
			EA:  [0]int{},
			L:   []int{1, 2, 3},
			O:   struct{ I int }{0},
			P:   &i,
			M:   map[int]int{1: 1},
			C:   CustomType("foo"),
			CP:  nil,
		}
		data, err := Marshal(v)
		if err != nil {
			t.Error(err)
		} else if string(data) != marshalExpect {
			t.Error(string(data))
		}
	})
	t.Run("List", func(t *testing.T) {
		t.Parallel()
		v := ListObject{
			B: [1]bool{true},
			I: [1]int{1},
			U: [1]uint{1},
			F: [1]float32{1.5},
			S: [1]string{"foo"},
			O: [1]struct{ I int }{struct{ I int }{0}},
			L: [1][1]string{[1]string{"bar"}},
			M: [1]map[string]string{map[string]string{"A": "B"}},
		}
		data, err := Marshal(v)
		if err != nil {
			t.Error(err)
		} else if string(data) != listExpect {
			t.Error(string(data))
		}
	})
	t.Run("Map", func(t *testing.T) {
		t.Parallel()
		v := map[string]string{"A": "B"}
		data, err := Marshal(v)
		if err != nil {
			t.Error(err)
		} else if string(data) != "\"A\" = \"B\"\n" {
			t.Error(string(data))
		}
	})
	t.Run("Handle nil", func(t *testing.T) {
		t.Parallel()
		t.Run("Slice", func(t *testing.T) {
			t.Parallel()
			data, err := Marshal(map[int][]int{1: nil})
			if err != nil {
				t.Error(err)
			} else if len(data) != 0 {
				t.Error(data)
			}
		})

		t.Run("Map", func(t *testing.T) {
			t.Parallel()
			data, err := Marshal(map[int]map[int]int{1: nil})
			if err != nil {
				t.Error(err)
			} else if len(data) != 0 {
				t.Error(data)
			}
		})

		t.Run("Ptr", func(t *testing.T) {
			t.Parallel()
			data, err := Marshal(map[int]*int{1: nil})
			if err != nil {
				t.Error(err)
			} else if len(data) != 0 {
				t.Error(data)
			}
		})
	})
}

type WriteError bool

func (w *WriteError) Write(p []byte) (int, error) {
	if *w {
		*w = false
		return len(p), nil
	}
	return 0, errors.New("write error")
}

func TestEncode(t *testing.T) {
	t.Parallel()
	t.Run("Handle error", func(t *testing.T) {
		t.Parallel()
		t.Run("Object", func(t *testing.T) {
			t.Parallel()
			w := WriteError(false)
			enc := NewEncoder(&w)
			err := enc.Encode(map[string]TestObject{"A": TestObject{}})
			if err == nil {
				t.Fail()
			} else if err.Error() != "write error" {
				t.Error(err)
			}
		})

		t.Run("Array", func(t *testing.T) {
			t.Parallel()
			w := WriteError(true)
			enc := NewEncoder(&w)
			err := enc.Encode(map[string][]int{"A": []int{0}})
			if err == nil {
				t.Fail()
			} else if err.Error() != "write error" {
				t.Error(err)
			}
		})

		t.Run("Outer object", func(t *testing.T) {
			t.Parallel()
			w := WriteError(false)
			enc := NewEncoder(&w)
			err := enc.Encode(struct{ A []int }{A: []int{0}})
			if err == nil {
				t.Fail()
			} else if err.Error() != "write error" {
				t.Error(err)
			}
		})

		t.Run("Map", func(t *testing.T) {
			t.Parallel()
			w := WriteError(false)
			enc := NewEncoder(&w)
			err := enc.Encode(map[int]map[int]int{0: map[int]int{0: 0}})
			if err == nil {
				t.Fail()
			} else if err.Error() != "write error" {
				t.Error(err)
			}
		})

		t.Run("Custom type", func(t *testing.T) {
			t.Parallel()
			w := WriteError(true)
			enc := NewEncoder(&w)
			err := enc.Encode(map[int][]CustomType{1: []CustomType{CustomType("")}})
			if err == nil {
				t.Fail()
			} else if err.Error() != "write error" {
				t.Error(err)
			}
		})
	})
	t.Run("Partial encode", func(t *testing.T) {
		t.Parallel()
		t.Run("Object", func(t *testing.T) {
			t.Parallel()
			var buf bytes.Buffer
			enc := NewEncoder(&buf)
			enc.EncodeToken(Value{Name: "A", Value: Object{}})
			err := enc.Encode(map[string]string{"B": "C"})
			if err != nil {
				t.Error(err)
			}
			enc.EncodeToken(EndObject{})
			if string(buf.Bytes()) != "\"A\" {\n\t\"B\" = \"C\"\n}\n" {
				t.Error(string(buf.Bytes()))
			}
		})
		t.Run("Array", func(t *testing.T) {
			t.Parallel()
			var buf bytes.Buffer
			enc := NewEncoder(&buf)
			enc.EncodeToken(Value{Name: "A", Value: Array{}})
			err := enc.Encode([]string{"B"})
			if err != nil {
				t.Error(err)
			}
			enc.EncodeToken(EndArray{})
			if string(buf.Bytes()) != "\"A\" [\n\t* \"B\"\n]\n" {
				t.Error(string(buf.Bytes()))
			}
		})
	})
}

const deepExpect = `A [
	{
		B [
		]
	}
]
`

func TestFinish(t *testing.T) {
	t.Parallel()
	t.Run("Object", func(t *testing.T) {
		t.Parallel()
		var buf bytes.Buffer
		enc := NewEncoder(&buf)
		enc.EncodeToken(Value{Name: "A", Value: Object{}})
		err := enc.Finish()
		if err != nil {
			t.Error(err)
		} else if string(buf.Bytes()) == "A {\n}" {
			t.Fail()
		}
	})
	t.Run("Array", func(t *testing.T) {
		t.Parallel()
		var buf bytes.Buffer
		enc := NewEncoder(&buf)
		enc.EncodeToken(Value{Name: "A", Value: Array{}})
		err := enc.Finish()
		if err != nil {
			t.Error(err)
		} else if string(buf.Bytes()) == "A [\n]" {
			t.Fail()
		}
	})
	t.Run("Deep", func(t *testing.T) {
		t.Parallel()
		var buf bytes.Buffer
		enc := NewEncoder(&buf)
		enc.EncodeToken(Value{Name: "A", Value: Array{}})
		enc.EncodeToken(Object{})
		enc.EncodeToken(Value{Name: "B", Value: Array{}})
		err := enc.Finish()
		if err != nil {
			t.Error(err)
		} else if string(buf.Bytes()) == deepExpect {
			t.Fail()
		}
	})
	t.Run("Handles error", func(t *testing.T) {
		t.Parallel()
		w := WriteError(true)
		enc := NewEncoder(&w)
		enc.EncodeToken(Value{Name: "A", Value: Array{}})
		err := enc.Finish()
		if err == nil {
			t.Fail()
		} else if err.Error() != "write error" {
			t.Error(err)
		}
	})
}
