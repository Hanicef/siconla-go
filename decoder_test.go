package siconla

import (
	"errors"
	"io"
	"strings"
	"testing"
)

const simpleInput = `
# comment

a = lorem
b {
	c = "ipsum"
}

d [
	= 'dolor'
	{
		e * "sit"
	}
	[
		* "amet"
	]
]
`

const stringInput = `
'foo' = "\a\b\f\n\r\t\v\'\"\\"
'bar' = '\x23\u2a\U2B'
`

func TestInputLine(t *testing.T) {
	t.Parallel()
	dec := NewDecoder(strings.NewReader(simpleInput))
	if dec.InputLine() != 0 {
		t.Fail()
	}

	lines := []int64{
		4, 5, 6, 7, 9, 10, 11, 12, 13, 14, 15, 16, 17,
	}

	for _, l := range lines {
		_, err := dec.Token()
		if err != nil {
			t.Error(err)
			return
		} else if dec.InputLine() != l {
			t.Errorf("%d != %d", dec.InputLine(), l)
		}
	}
}

func TestToken(t *testing.T) {
	t.Parallel()
	t.Run("Simple", func(t *testing.T) {
		t.Parallel()
		dec := NewDecoder(strings.NewReader(simpleInput))

		tok, err := dec.Token()
		if err != nil {
			t.Error(err)
		} else {
			v, ok := tok.(Value)
			if !ok || v.Name != "a" {
				t.Error(tok)
			}
			s, ok := v.Value.(string)
			if !ok || s != "lorem" {
				t.Error(v.Value)
			}
		}

		tok, err = dec.Token()
		if err != nil {
			t.Error(err)
		} else {
			v, ok := tok.(Value)
			if !ok || v.Name != "b" {
				t.Error(tok)
			}
			if _, ok = v.Value.(Object); !ok {
				t.Fail()
			}
		}

		tok, err = dec.Token()
		if err != nil {
			t.Error(err)
		} else {
			v, ok := tok.(Value)
			if !ok || v.Name != "c" {
				t.Error(tok)
			}
			s, ok := v.Value.(string)
			if !ok || s != "ipsum" {
				t.Fail()
			}
		}

		tok, err = dec.Token()
		if err != nil {
			t.Error(err)
		} else if _, ok := tok.(EndObject); !ok {
			t.Error(tok)
		}

		tok, err = dec.Token()
		if err != nil {
			t.Error(err)
		} else {
			v, ok := tok.(Value)
			if !ok || v.Name != "d" {
				t.Error(tok)
			}
			if _, ok = v.Value.(Array); !ok {
				t.Fail()
			}
		}

		tok, err = dec.Token()
		if err != nil {
			t.Error(err)
		} else if s, ok := tok.(string); !ok || s != "dolor" {
			t.Error(tok)
		}

		tok, err = dec.Token()
		if err != nil {
			t.Error(err)
		} else if _, ok := tok.(Object); !ok {
			t.Error(tok)
		}

		tok, err = dec.Token()
		if err != nil {
			t.Error(err)
		} else {
			v, ok := tok.(Value)
			if !ok || v.Name != "e" {
				t.Error(tok)
			}
			s, ok := v.Value.(string)
			if !ok || s != "sit" {
				t.Fail()
			}
		}

		tok, err = dec.Token()
		if err != nil {
			t.Error(err)
		} else if _, ok := tok.(EndObject); !ok {
			t.Error(tok)
		}

		tok, err = dec.Token()
		if err != nil {
			t.Error(err)
		} else if _, ok := tok.(Array); !ok {
			t.Error(tok)
		}

		tok, err = dec.Token()
		if err != nil {
			t.Error(err)
		} else if s, ok := tok.(string); !ok || s != "amet" {
			t.Error(tok)
		}

		tok, err = dec.Token()
		if err != nil {
			t.Error(err)
		} else if _, ok := tok.(EndArray); !ok {
			t.Error(tok)
		}

		tok, err = dec.Token()
		if err != nil {
			t.Error(err)
		} else if _, ok := tok.(EndArray); !ok {
			t.Error(tok)
		}

		_, err = dec.Token()
		if !errors.Is(err, io.EOF) {
			t.Error(err)
		}
	})
	t.Run("String formatting", func(t *testing.T) {
		t.Parallel()
		dec := NewDecoder(strings.NewReader(stringInput))

		tok, err := dec.Token()
		if err != nil {
			t.Error(err)
		} else {
			v, ok := tok.(Value)
			if !ok || v.Name != "foo" {
				t.Error(tok)
			}
			s, ok := v.Value.(string)
			if !ok || s != "\a\b\f\n\r\t\v'\"\\" {
				t.Error(v.Value)
			}
		}

		tok, err = dec.Token()
		if err != nil {
			t.Error(err)
		} else {
			v, ok := tok.(Value)
			if !ok || v.Name != "bar" {
				t.Error(tok)
			}
			s, ok := v.Value.(string)
			if !ok || s != "#*+" {
				t.Error(v.Value)
			}
		}
	})
	t.Run("Missing open bracket", func(t *testing.T) {
		t.Parallel()
		var e *ParserError

		dec := NewDecoder(strings.NewReader("}"))
		_, err := dec.Token()
		if err == nil {
			t.Error(err)
		} else if !errors.As(err, &e) || e.Error() != "1: mismatching closing bracket" {
			t.Error(err)
		}
		_, nerr := dec.Token()
		if err != nerr {
			t.Error(nerr)
		}

		dec = NewDecoder(strings.NewReader("]"))
		_, err = dec.Token()
		if err == nil {
			t.Error(err)
		} else if !errors.As(err, &e) || e.Error() != "1: mismatching closing bracket" {
			t.Error(err)
		}
		_, nerr = dec.Token()
		if err != nerr {
			t.Error(nerr)
		}
	})
	t.Run("Bracket mismatch", func(t *testing.T) {
		t.Parallel()
		var e *ParserError

		dec := NewDecoder(strings.NewReader("foo {\n]"))
		dec.Token()
		_, err := dec.Token()
		if err == nil {
			t.Error(err)
		} else if !errors.As(err, &e) || e.Error() != "2: mismatching closing bracket" {
			t.Error(err)
		}
		_, nerr := dec.Token()
		if err != nerr {
			t.Error(nerr)
		}

		dec = NewDecoder(strings.NewReader("foo [\n}"))
		dec.Token()
		_, err = dec.Token()
		if err == nil {
			t.Error(err)
		} else if !errors.As(err, &e) || e.Error() != "2: mismatching closing bracket" {
			t.Error(err)
		}
		_, nerr = dec.Token()
		if err != nerr {
			t.Error(nerr)
		}
	})
	t.Run("Bracket not closed", func(t *testing.T) {
		t.Parallel()
		var e *ParserError

		dec := NewDecoder(strings.NewReader("foo {"))
		dec.Token()
		_, err := dec.Token()
		if err == nil {
			t.Fail()
		} else if !errors.As(err, &e) || e.Error() != "1: missing closing bracket" {
			t.Error(err)
		}
		_, nerr := dec.Token()
		if err != nerr {
			t.Error(nerr)
		}

		dec = NewDecoder(strings.NewReader("foo ["))
		dec.Token()
		_, err = dec.Token()
		if err == nil {
			t.Fail()
		} else if !errors.As(err, &e) || e.Error() != "1: missing closing bracket" {
			t.Error(err)
		}
		_, nerr = dec.Token()
		if err != nerr {
			t.Error(nerr)
		}
	})
	t.Run("Missing value", func(t *testing.T) {
		t.Parallel()
		var e *ParserError

		dec := NewDecoder(strings.NewReader("foo"))
		_, err := dec.Token()
		if err == nil {
			t.Fail()
		} else if !errors.As(err, &e) || e.Error() != "1: missing value" {
			t.Error(err)
		}
		_, nerr := dec.Token()
		if err != nerr {
			t.Error(nerr)
		}

		dec = NewDecoder(strings.NewReader("foo {\nbar\n}"))
		dec.Token()
		_, err = dec.Token()
		if err == nil {
			t.Fail()
		} else if !errors.As(err, &e) || e.Error() != "2: missing value" {
			t.Error(err)
		}
		_, nerr = dec.Token()
		if err != nerr {
			t.Error(nerr)
		}

		dec = NewDecoder(strings.NewReader("foo [\nbar\n]"))
		dec.Token()
		_, err = dec.Token()
		if err == nil {
			t.Fail()
		} else if !errors.As(err, &e) || e.Error() != "2: unknown token" {
			t.Error(err)
		}
		_, nerr = dec.Token()
		if err != nerr {
			t.Error(nerr)
		}

		dec = NewDecoder(strings.NewReader("\"foo\" bar"))
		dec.Token()
		_, err = dec.Token()
		if err == nil {
			t.Fail()
		} else if !errors.As(err, &e) || e.Error() != "1: unknown token" {
			t.Error(err)
		}
		_, nerr = dec.Token()
		if err != nerr {
			t.Error(nerr)
		}
	})
	t.Run("Missing closing quote", func(t *testing.T) {
		t.Parallel()
		var e *ParserError

		dec := NewDecoder(strings.NewReader("foo = 'foo"))
		_, err := dec.Token()
		if err == nil {
			t.Error(err)
		} else if !errors.As(err, &e) || e.Error() != "1: missing closing quote" {
			t.Error(err)
		}
		_, nerr := dec.Token()
		if err != nerr {
			t.Error(nerr)
		}

		dec = NewDecoder(strings.NewReader("foo = \"foo"))
		_, err = dec.Token()
		if err == nil {
			t.Error(err)
		} else if !errors.As(err, &e) || e.Error() != "1: missing closing quote" {
			t.Error(err)
		}
		_, nerr = dec.Token()
		if err != nerr {
			t.Error(nerr)
		}

		dec = NewDecoder(strings.NewReader("foo = \"foo\\"))
		_, err = dec.Token()
		if err == nil {
			t.Error(err)
		} else if !errors.As(err, &e) || e.Error() != "1: missing closing quote" {
			t.Error(err)
		}
		_, nerr = dec.Token()
		if err != nerr {
			t.Error(nerr)
		}

		dec = NewDecoder(strings.NewReader("foo = \"foo\\\""))
		_, err = dec.Token()
		if err == nil {
			t.Error(err)
		} else if !errors.As(err, &e) || e.Error() != "1: missing closing quote" {
			t.Error(err)
		}
		_, nerr = dec.Token()
		if err != nerr {
			t.Error(nerr)
		}

		dec = NewDecoder(strings.NewReader("foo = \"foo\\x"))
		_, err = dec.Token()
		if err == nil {
			t.Error(err)
		} else if !errors.As(err, &e) || e.Error() != "1: missing closing quote" {
			t.Error(err)
		}
		_, nerr = dec.Token()
		if err != nerr {
			t.Error(nerr)
		}

		dec = NewDecoder(strings.NewReader("foo = \"foo\\u"))
		_, err = dec.Token()
		if err == nil {
			t.Error(err)
		} else if !errors.As(err, &e) || e.Error() != "1: missing closing quote" {
			t.Error(err)
		}
		_, nerr = dec.Token()
		if err != nerr {
			t.Error(nerr)
		}

		dec = NewDecoder(strings.NewReader("a [\n= \"foo\n]"))
		dec.Token()
		_, err = dec.Token()
		if err == nil {
			t.Error(err)
		} else if !errors.As(err, &e) || e.Error() != "2: missing closing quote" {
			t.Error(err)
		}
		_, nerr = dec.Token()
		if err != nerr {
			t.Error(nerr)
		}
	})
	t.Run("Bad hex sequence", func(t *testing.T) {
		t.Parallel()
		var e *ParserError
		dec := NewDecoder(strings.NewReader("foo = \"\\xzz\""))
		_, err := dec.Token()
		if err == nil {
			t.Fail()
		} else if !errors.As(err, &e) || e.Error() != "1: malformatted hex value" {
			t.Error(err)
		}
		_, nerr := dec.Token()
		if err != nerr {
			t.Error(nerr)
		}
	})
	t.Run("Bad unicode sequence", func(t *testing.T) {
		t.Parallel()
		var e *ParserError

		dec := NewDecoder(strings.NewReader("foo = \"\\uzzzz\""))
		_, err := dec.Token()
		if err == nil {
			t.Fail()
		} else if !errors.As(err, &e) || e.Error() != "1: malformatted unicode value" {
			t.Error(err)
		}
		_, nerr := dec.Token()
		if err != nerr {
			t.Error(nerr)
		}

		dec = NewDecoder(strings.NewReader("foo = \"\\Uzzzzzzzz\""))
		_, err = dec.Token()
		if err == nil {
			t.Fail()
		} else if !errors.As(err, &e) || e.Error() != "1: malformatted unicode value" {
			t.Error(err)
		}
		_, nerr = dec.Token()
		if err != nerr {
			t.Error(nerr)
		}
	})
	t.Run("Bad escape sequence", func(t *testing.T) {
		t.Parallel()
		var e *ParserError

		dec := NewDecoder(strings.NewReader("\"\\z\" = 'bar'"))
		_, err := dec.Token()
		if err == nil {
			t.Fail()
		} else if !errors.As(err, &e) || e.Error() != "1: unknown escape sequence" {
			t.Error(err)
		}
		_, nerr := dec.Token()
		if err != nerr {
			t.Error(nerr)
		}
	})
	t.Run("Trailing characters", func(t *testing.T) {
		t.Parallel()

		tests := map[string]string{
			"String":     "\na = b # error",
			"Quote":      "\na = 'b' error",
			"Object":     "\na { error",
			"Array":      "\na [ error",
			"End object": "a {\n} error",
			"End array":  "a [\n] error",
		}
		for name, test := range tests {
			t.Run(name, func(name, test string) func(t *testing.T) {
				return func(t *testing.T) {
					t.Parallel()
					var e *ParserError

					dec := NewDecoder(strings.NewReader(test))
					_, err := dec.Token()
					if err == nil {
						t.Fail()
					} else if !errors.As(err, &e) || e.Error() != "2: expected EOL" {
						t.Error(err)
					}
					_, nerr := dec.Token()
					if err != nerr {
						t.Error(nerr)
					}
				}
			}(name, test))
		}
	})
}

type CustomType string

func (c *CustomType) UnmarshalSiconla(dec *Decoder, tok Token) error {
	if s := tok.(string); s != "" {
		if c != nil {
			*c = CustomType(s)
		}
		return nil
	}
	return errors.New("fail")
}

type TestObject struct {
	T   bool
	F   bool
	I   int
	I8  int8
	I16 int16
	I32 int32
	I64 int64
	U   uint
	U8  uint8
	U16 uint16
	U32 uint32
	U64 uint64
	F32 float32
	F64 float64
	S   string
	A   [3]int
	EA  [0]int
	L   []int
	O   struct {
		I int
	}
	P  *int
	M  map[int]int
	C  CustomType
	CP *CustomType
}

type BadObject struct {
	F interface{}
}

func TestUnmarshal(t *testing.T) {
	t.Parallel()
	t.Run("Simple", func(t *testing.T) {
		t.Parallel()
		s := `
		T = true
		F = false
		I = 0b1
		I8 = 2
		I16 = 3
		I32 = 4
		I64 = -5
		U = 6
		U8 = 7
		U16 = 010
		U32 = 9
		U64 = 0xa
		F32 = 1.5
		F64 = 1.75
		S = "hello"
		A [
			* 1
			* 2
		]
		EA [
		]
		L [
			* 1
			* 2
			* 3
		]
		O {
			I = 4
		}
		P = 5
		M {
			1 = 3
			2 = 4
		}
		C = 'test'
		CP = 'not reached'
		`
		v := TestObject{}
		err := Unmarshal([]byte(s), &v)
		if err != nil {
			t.Error(err)
		} else if v.T != true {
			t.Error(v.T)
		} else if v.F != false {
			t.Error(v.F)
		} else if v.I != 1 {
			t.Error(v.I)
		} else if v.I8 != 2 {
			t.Error(v.I8)
		} else if v.I16 != 3 {
			t.Error(v.I16)
		} else if v.I32 != 4 {
			t.Error(v.I32)
		} else if v.I64 != -5 {
			t.Error(v.I64)
		} else if v.U != 6 {
			t.Error(v.U)
		} else if v.U8 != 7 {
			t.Error(v.U8)
		} else if v.U16 != 8 {
			t.Error(v.U16)
		} else if v.U32 != 9 {
			t.Error(v.U32)
		} else if v.U64 != 10 {
			t.Error(v.U64)
		} else if v.F32 != 1.5 {
			t.Error(v.F32)
		} else if v.F64 != 1.75 {
			t.Error(v.F64)
		} else if v.S != "hello" {
			t.Error(v.S)
		} else if v.A[0] != 1 {
			t.Error(v.A)
		} else if v.A[1] != 2 {
			t.Error(v.A)
		} else if v.L[0] != 1 {
			t.Error(v.L)
		} else if v.L[1] != 2 {
			t.Error(v.L)
		} else if v.L[2] != 3 {
			t.Error(v.L)
		} else if v.O.I != 4 {
			t.Error(v.O.I)
		} else if *v.P != 5 {
			t.Error(*v.P)
		} else if v.M[1] != 3 {
			t.Error(v.M)
		} else if v.M[2] != 4 {
			t.Error(v.M)
		} else if v.C != "test" {
			t.Error(v.C)
		}
	})
	t.Run("Bad values", func(t *testing.T) {
		t.Parallel()
		tests := map[string]string{
			"Empty bool": "T = ''",
			"Bool":       "F = error",
			"Int":        "I = NaN",
			"Int8":       "I8 = NaN",
			"Int16":      "I16 = NaN",
			"Int32":      "I32 = NaN",
			"Int64":      "I64 = NaN",
			"Uint":       "U = -1",
			"Uint8":      "U8 = -1",
			"Uint16":     "U16 = -1",
			"Uint32":     "U32 = -1",
			"Uint64":     "U64 = -1",
			"Float32":    "F32 = nil",
			"Float64":    "F64 = nil",
			"Custom":     "C = ''",
		}
		errors := map[string]string{
			"Empty bool": "1: cannot set value '' to bool",
			"Bool":       "1: cannot set value 'error' to bool",
			"Int":        "1: cannot set value 'NaN' to int",
			"Int8":       "1: cannot set value 'NaN' to int",
			"Int16":      "1: cannot set value 'NaN' to int",
			"Int32":      "1: cannot set value 'NaN' to int",
			"Int64":      "1: cannot set value 'NaN' to int",
			"Uint":       "1: cannot set value '-1' to uint",
			"Uint8":      "1: cannot set value '-1' to uint",
			"Uint16":     "1: cannot set value '-1' to uint",
			"Uint32":     "1: cannot set value '-1' to uint",
			"Uint64":     "1: cannot set value '-1' to uint",
			"Float32":    "1: cannot set value 'nil' to float",
			"Float64":    "1: cannot set value 'nil' to float",
			"Custom":     "1: fail",
		}
		for name, test := range tests {
			t.Run(name, func(name, test string) func(t *testing.T) {
				return func(t *testing.T) {
					t.Parallel()
					v := TestObject{}
					err := Unmarshal([]byte(test), &v)
					if err == nil {
						t.Fail()
					} else if err.Error() != errors[name] {
						t.Error(err)
					}
				}
			}(name, test))
		}
	})
	t.Run("Type mismatch", func(t *testing.T) {
		t.Parallel()
		tests := map[string]string{
			"Bool":        "T {",
			"Int":         "I {",
			"Int8":        "I8 {",
			"Int16":       "I16 {",
			"Int32":       "I32 {",
			"Int64":       "I64 {",
			"Uint":        "U {",
			"Uint8":       "U8 {",
			"Uint16":      "U16 {",
			"Uint32":      "U32 {",
			"Uint64":      "U64 {",
			"Float32":     "F32 {",
			"Float64":     "F64 {",
			"String":      "S {",
			"Array":       "A = 0xa",
			"Empty array": "EA = 0xa",
			"Slice":       "L = 0xa",
			"Ptr":         "P {",
			"Object":      "O [",
			"Map":         "M = 0xa",
		}
		errors := map[string]string{
			"Bool":        "1: expected string",
			"Int":         "1: expected string",
			"Int8":        "1: expected string",
			"Int16":       "1: expected string",
			"Int32":       "1: expected string",
			"Int64":       "1: expected string",
			"Uint":        "1: expected string",
			"Uint8":       "1: expected string",
			"Uint16":      "1: expected string",
			"Uint32":      "1: expected string",
			"Uint64":      "1: expected string",
			"Float32":     "1: expected string",
			"Float64":     "1: expected string",
			"String":      "1: expected string",
			"Array":       "1: expected array",
			"Empty array": "1: expected array",
			"Slice":       "1: expected array",
			"Ptr":         "1: expected string",
			"Object":      "1: expected object",
			"Map":         "1: expected object",
		}
		for name, test := range tests {
			t.Run(name, func(name, test string) func(t *testing.T) {
				return func(t *testing.T) {
					t.Parallel()
					v := TestObject{}
					err := Unmarshal([]byte(test), &v)
					if err == nil {
						t.Fail()
					} else if err.Error() != errors[name] {
						t.Error(err)
					}
				}
			}(name, test))
		}
	})
	t.Run("Unexpected EOF", func(t *testing.T) {
		t.Parallel()
		tests := map[string]string{
			"Array":       "A [",
			"Empty array": "EA [",
			"Slice":       "L [",
			"Object":      "O {",
			"Map":         "M {",
		}
		errors := map[string]string{
			"Array":       "1: missing closing bracket",
			"Empty array": "1: missing closing bracket",
			"Slice":       "1: missing closing bracket",
			"Object":      "1: missing closing bracket",
			"Map":         "1: missing closing bracket",
		}
		for name, test := range tests {
			t.Run(name, func(name, test string) func(t *testing.T) {
				return func(t *testing.T) {
					t.Parallel()
					v := TestObject{}
					err := Unmarshal([]byte(test), &v)
					if err == nil {
						t.Fail()
					} else if err.Error() != errors[name] {
						t.Error(err)
					}
				}
			}(name, test))
		}
	})
	t.Run("Faulty values", func(t *testing.T) {
		t.Parallel()
		tests := map[string]string{
			"Array":     "A [\n= fail\n]",
			"Slice":     "L [\n= fail\n]",
			"Object":    "O {\nI = fail\n}",
			"Map key":   "M {\nfail = 1\n}",
			"Map value": "M {\n1 = fail\n}",
		}
		for name, test := range tests {
			t.Run(name, func(test string) func(t *testing.T) {
				return func(t *testing.T) {
					t.Parallel()
					v := TestObject{}
					err := Unmarshal([]byte(test), &v)
					if err == nil {
						t.Fail()
					} else if err.Error() != "2: cannot set value 'fail' to int" {
						t.Error(err)
					}
				}
			}(test))
		}
	})
	t.Run("Unknown key", func(t *testing.T) {
		t.Parallel()
		v := TestObject{}
		err := Unmarshal([]byte("Z = hello"), &v)
		if err != nil {
			t.Error(err)
		}
	})
	t.Run("Excessive values", func(t *testing.T) {
		t.Parallel()
		s := `A [
			* 1
			* 2
			* 3
			* 4
		]`
		v := TestObject{}
		err := Unmarshal([]byte(s), &v)
		if err == nil {
			t.Fail()
		} else if err.Error() != "5: too many values" {
			t.Error(err)
		}
	})
	t.Run("Non-pointer input", func(t *testing.T) {
		t.Parallel()
		defer func() {
			err := recover()
			if err == nil {
				t.Fail()
			} else if err != "v must be a pointer" {
				t.Error(err)
			}
		}()
		v := TestObject{}
		Unmarshal([]byte("Z = hello"), v)
	})
	t.Run("Non-struct input", func(t *testing.T) {
		t.Parallel()
		var v map[string]string
		err := Unmarshal([]byte("Z = hello"), &v)
		if err != nil {
			t.Error(err)
		} else if v["Z"] != "hello" {
			t.Error(v)
		}
	})
	t.Run("Bad type", func(t *testing.T) {
		t.Parallel()
		defer func() {
			err := recover()
			if err == nil {
				t.Fail()
			} else if err != "cannot unmarshal type interface" {
				t.Error(err)
			}
		}()
		v := BadObject{}
		Unmarshal([]byte("F = 'nil'"), &v)
	})
}

func TestDecode(t *testing.T) {
	t.Parallel()
	t.Run("Partial read", func(t *testing.T) {
		t.Parallel()
		dec := NewDecoder(strings.NewReader("A [\n= 1\n= 2\n= 3\n]"))
		dec.Token()
		var a []int
		err := dec.Decode(&a)
		if err != nil {
			t.Error(err)
		} else if a[0] != 1 || a[1] != 2 || a[2] != 3 {
			t.Error(a)
		}
	})
	t.Run("Unsupported type", func(t *testing.T) {
		t.Parallel()
		defer func() {
			err := recover()
			if err == nil {
				t.Fail()
			} else if err != "cannot unmarshal type interface" {
				t.Error(err)
			}
		}()
		dec := NewDecoder(strings.NewReader("A = hello"))
		var v interface{}
		dec.Decode(&v)
	})
}
