package siconla

type (
	// A Token represents any of the following types:
	//
	//     Object
	//     EndObject
	//     Array
	//     EndArray
	//     Value
	//     string
	Token interface{}

	// An Object represents the beginning of a Siconla object.
	Object struct{}

	// An EndObject represents the end of a Siconla object.
	EndObject struct{}

	// An Array represents the beginning of a Siconla array.
	Array struct{}

	// An EndArray represents the end of a Siconla array.
	EndArray struct{}

	// A Value represents a single key-value pair in a Siconla object.
	Value struct {
		Name  string
		Value Token
	}
)
